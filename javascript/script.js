var foods = ["asparagus","apples","avocado","alfalfa","acorn squash",
            "bruscetta","bacon","black beans","bagels","baked beans",
            "cabbage","cake","carrots","carne asada","celery",
            "dates","dips","duck","dumplings","donuts",
            "eggs","enchilada","eggrolls","english muffins","edimame",
            "fajita","falafel","fish","franks","fondu",
            "garlic","ginger","gnocchi","goose","granola",
            "ham","halibut","hamburger","honey","huenos rancheros",
            "ice cream","irish stew","indian food","italian bread",
            "jambalaya","jelly","jerky","jalapeno",
            "kale","kabobs","ketchup","kiwi","kidney beans",
            "lobster","lamb","linguine","lasagna",
            "meatballs","moose","milk","milkshake",
            "noodles",
            "ostrich",
            "pizza","pepperoni","porter","pussy","pancakes",
            "quesadilla","quiche",
            "reuben",
            "spinach","spaghetti",
            "tater tots","toast",
            "venison",
            "waffles","wine","walnuts",
            "yogurt",
            "ziti","zucchini" ]

var adjectives = [
            "Good","New","First","Last","Long","great","little","own",
            "other","old","right","big","high","different","small","large",
            "next","early","young","important","few","public","bad","same","able" ]

function randAdjective() {
    var x = Math.floor(Math.random() * 25); 
    var y = adjectives[x];
    var z = y.toUpperCase();
    document.getElementById("adjective").innerHTML = z;
}

function randFood() {
    var x = Math.floor(Math.random() * 82);    
    var y = foods[x];    
    var z =  y.toUpperCase();
    document.getElementById("number").innerHTML = z;
}
